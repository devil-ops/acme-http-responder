#!/bin/bash

if [ $# -ne 2 ]; then
  echo "Usage: ${0} <docker host> <docker image>" >&2
  exit 1
fi

DOCKER=${1}
IMAGE=${2}
EXIT_CODE=0

function test_response() {
  local test_url=${1}
  local expected_code=${2}
  local expected_redir_url=${3}

  response=$(curl -s -o /dev/null -w '%{http_code} %{redirect_url}' ${test_url})
  expected_response="${expected_code} ${expected_redir_url}"

  if [ "${response}" != "${expected_response}" ]; then
    EXIT_CODE=1
    echo "ERROR: Expected '${expected_response}', got: '${response}'"
  else
    echo "PASS"
  fi
}


echo "Starting test container"
container_id=$(docker run --rm -d -p 80:80 ${IMAGE})
echo "Sleeping for container to start..."
sleep 1
docker exec ${container_id} mkdir -p /webroot/.well-known/acme-challenge
docker exec ${container_id} sh -c 'echo token_value > /webroot/.well-known/acme-challenge/test_token'

echo -n "Testing redirect to HTTPS:       "
test_response http://${DOCKER}/testurl 301 https://${DOCKER}/testurl

echo -n "Testing 200 for acme challenge:  "
test_response http://${DOCKER}/.well-known/acme-challenge/test_token 200

echo -n "Testing 404 for acme challenge:  "
test_response http://${DOCKER}/.well-known/acme-challenge/bad_token 404

echo ''
if [ ${EXIT_CODE} != 0 ]; then
  echo "Container logs:"
  docker logs ${container_id}
  echo ""
fi
echo "Stopping container"
docker stop ${container_id}


echo ''
echo "Starting test container with ACME_NEXT_HOST"

container_id=$(docker run --rm -d -p 80:80 --env ACME_NEXT_HOST=b.example.com ${IMAGE})
echo "Sleeping for container to start..."
sleep 1
docker exec ${container_id} mkdir -p /webroot/.well-known/acme-challenge
docker exec ${container_id} sh -c 'echo token_value > /webroot/.well-known/acme-challenge/test_token'

echo -n "Testing redirect to HTTPS:       "
test_response http://${DOCKER}/testurl 301 https://${DOCKER}/testurl

echo -n "Testing 200 for acme challenge:  "
test_response http://${DOCKER}/.well-known/acme-challenge/test_token 200

echo -n "Testing 402 for acme challenge:  "
test_response http://${DOCKER}/.well-known/acme-challenge/bad_token 302 http://b.example.com/.well-known/acme-challenge/bad_token

echo ''
if [ ${EXIT_CODE} != 0 ]; then
  echo "Container logs:"
  docker logs ${container_id}
  echo ""
fi
echo "Stopping container"
docker stop ${container_id}




exit ${EXIT_CODE}
