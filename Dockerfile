FROM nginx:1-alpine

ENV ACME_NEXT_HOST=
COPY nginx-templates/default.conf.template /etc/nginx/templates/default.conf.template
