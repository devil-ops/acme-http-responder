# acme-http-responder

This is a light-weight container to handle ACME http responses and HTTP-to-HTTPS redirects.

This container also supports multiple hosts that respond to the same DNS name. (see below)

## Use Cases

### HTTP-to-HTTPS redirect (with ACME responses)

This allows you to have a single port 80 responder on your host regardless of what web
services you are running.  We've seen cases where ACME certs fail to renew because certbot
used a temporary webserver to get the initial cert, then another webserver was stood up
later that blocked the temporary webserver from starting from renewal.  With this container,
you can have the same port 80 listener the whole time.

When a client connects to port 80 for anything but an ACME challenge, it will be
redirected to the same URL, but with `https` instead of `http`.

To run the container:

```bash
docker run -d -p 80:80 -v /var/run/acme-webroot:/webroot --restart always \
  --name acme-http-responder gitlab-registry.oit.duke.edu/devil-ops/acme-http-responder:latest
```

or with docker-compose:

```yaml
services:
  acme-http-responder:
    image: gitlab-registry.oit.duke.edu/devil-ops/acme-http-responder:latest
    restart: always
    volumes:
      - /var/run/acme-webroot:/webroot
    ports:
      - 80:80
```
**NOTE:** You may have to add a `:z` to the end of your volume mounts if SELinux is running.

Once the container is running, you can request a cert (other ACME clients should work similarly):
```
certbot --webroot -w /var/run/acme-webroot \
  --server https://locksmith.oit.duke.edu/acme/v2/directory \
  --email contact-email@duke.edu --agree-tos --no-eff-email -d fqdn.oit.duke.edu certonly
```

### Multiple hosts sharing a name

Sometimes we have multiple hosts that need a cert for the same name.  They could be
behind a load balanced or have a round robin DNS entry.  In either case, ACME's `http-01`
challenge response does not work well because you can't predict which host the ACME
server will connect to on port 80.

Lets say you have hosts `a.example.com`, `b.example.com`, and `c.example.com`.  They are
all part of the round-robin DNS for `myapp.example.com`.  If `a.example.com` is requesting
a cert for `myapp.example.com`, the ACME server might connect to `b.example.com` instead
and get a 404 error, causing the request to fail.

To work around this, we setup a redirect loop.  If `b.example.com` doesn't have the
response file on disk, it will redirect to the same URL on `c.example.com`.  If
`c.example.com` doesn't have the response file, it will redirect to `a.example.com`.  If
`a.example.com` doesn't have the response file, it will redirect to `b.example.com`.

To implement this, `a.example.com` would launch the acme-http-responder container with the
`ACME_NEXT_HOST` environment variable set to `b.example.com`.  The other hosts would
launch the container with similar arguments.

The container initialization might look like:

```bash
docker run -d -p 80:80 -v /var/run/acme-webroot:/webroot --restart always \
  --env ACME_NEXT_HOST=b.example.com \
  --name acme-http-responder gitlab-registry.oit.duke.edu/devil-ops/acme-http-responder:latest
```

or with docker-compose:

```yaml
services:
  acme-http-responder:
    image: gitlab-registry.oit.duke.edu/devil-ops/acme-http-responder:latest
    restart: always
    volumes:
      - /var/run/acme-webroot:/webroot
    ports:
      - 80:80
    environment:
      ACME_NEXT_HOST: b.example.com
```
**NOTE:** You may have to add a `:z` to the end of your volume mounts if SELinux is running.

Once the container is running, you can request a cert (other ACME clients should work similarly):
```
certbot --webroot -w /var/run/acme-webroot \
  --server https://locksmith.oit.duke.edu/acme/v2/directory \
  --email contact-email@duke.edu --agree-tos --no-eff-email \
  -d fqdn.oit.duke.edu -d shared-fqdn.oit.duke.edu certonly
```
